<?php
/*
Plugin Name: WPezPlugins: Theme Unit Test
Plugin URI:
Description:
Version: 0.0.0
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: wpez-tut
*/

namespace WPezThemeUnitTest;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

function defaults() {

	$arr = [
		'option_key' => __NAMESPACE__ . '_wpez_wp_test_notice',
		'notice_0'   => __( "Import the test data from: " . untrailingslashit( plugin_dir_path( __FILE__ ) ), 'wpez-tut' ),
		'style_div'  => 'background:#46b450',
		'style_p'    => 'color: #fff; font-size:17px',
	];

	return $arr;
}

register_activation_hook( __FILE__, __NAMESPACE__ . '\registerActivationHook' );
function registerActivationHook() {

	$arr_notices   = get_option( defaults()['option_key'], array() );
	$arr_notices[] = defaults()['notice_0'];
	update_option( defaults()['option_key'], $arr_notices );
}


add_action( 'admin_notices', __NAMESPACE__ . '\adminNotices' );
function adminNotices() {

	if ( $arr_notices = get_option( defaults()['option_key'] ) ) {
		foreach ( $arr_notices as $str_notice ) {

			echo '<div class="notice updated" style="' . esc_attr( defaults()['style_div'] ) . '">';
			echo '<p style="' . esc_attr( defaults()['style_p'] ) . '">' . esc_html( $str_notice ) . '</p>';
			echo '</div>';
		}
		delete_option( defaults()['option_key'] );
	}
}

register_deactivation_hook( __FILE__, 'registerDeactivationHook' );
function registerDeactivationHook() {
	delete_option( defaults()['option_key'] );
}