## WPezPlugins: Theme Unit Test

__The WPTRT theme-unit-test.xml pre-downloaded, stashed in this plugin, and ready to be imported.__

Also included is PostStatus' WPTest.io data.

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Overview

Import the WPTRT theme-unit-test.xml test data into your WP install for testing. Per WP.org, "To be clear, this is only one step in testing your Theme. See Theme testing for a full guide." 

### Getting Started 

Once you install this plugin - it does not have to be activated - go to: Admin > Tools > Import > WordPress. 

When you're selecting the file to import, navigate to wp-content/plugins/wpez-theme-unit-test/data.

Once you're within the data folder choose the wptrt-theme-unit-test folder or the poststatus-wptest folder and complete the import process.

 
 ### FAQ
 
 __1 - Why?__
 
Entering faux content, even basic faux content, is an annoyance (and a time suck). This is a simple shortcut. Basic, but still helpful.

 __2 - This doesn't follow the WP coding standards / naming convention?__
 
 Yup. Don't panic. That's okay. Everything is gonna be alright.
 
__3 - I'm not a developer, can we hire you?__
  
  Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 



### Helpful Links

- https://codex.wordpress.org/Theme_Development

- https://codex.wordpress.org/Theme_Unit_Test

- https://github.com/WPTRT/theme-unit-test

- http://wptest.io/

- https://github.com/poststatus/wptest


### TODO 



### CHANGE LOG

- v0.0.0 - 23 November 2019
   
   Proof of Concept

